<?php

namespace Drupal\azure_cdn_purge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Azure CDN settings for this purger.
 */
class AzureCdnSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'azure_cdn_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'azure_cdn_purge.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('azure_cdn_purge.settings');

    // Purge settings.
    $form['purge_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Purge settings'),
    ];

    $form['purge_settings']['endpoint_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Endpoint type'),
      '#options' => [
        'endpoints' => $this->t('Azure CDN'),
        'afdEndpoints' => $this->t('Azure FrontDoor'),
      ],
      '#required' => TRUE,
      '#default_value' => $config->get('endpoint_type'),
    ];

    $form['purge_settings']['devel'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debug mode'),
      '#default_value' => $config->get('devel'),
    ];

    $form['purge_settings']['chunk_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Purges per request'),
      '#attributes' => [
        'type' => 'number',
      ],
      '#description' => $this->t('Maximum number of purges per request.'),
      '#required' => TRUE,
      '#default_value' => $config->get('chunk_size'),
      '#maxlength' => 4,
    ];

    $form['purge_settings']['chunk_delay'] = [
      '#type' => 'textfield',
      '#title' => $this->t('How many seconds to wait between requests.'),
      '#attributes' => [
        'type' => 'number',
      ],
      '#description' => $this->t('Azure CDN/Frontdoor may throw an error if requests are sent too quickly.'),
      '#required' => TRUE,
      '#default_value' => $config->get('chunk_delay'),
      '#maxlength' => 4,
    ];

    // Authentication settings.
    $form['authentication'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Authentication - OAuth 2.0'),
    ];

    $form['authentication']['description'] = [
      '#markup' => '<div>' . $this->t('Please refer to <a href="https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-client-creds-grant-flow#first-case-access-token-request-with-a-shared-secret" target="_blank">Access token request with a shared secret</a>.') . '</div>',
    ];

    $form['authentication']['tenant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tenant ID'),
      '#description' => $this->t('Used to control who can sign into the application.'),
      '#required' => TRUE,
      '#default_value' => $config->get('tenant_id'),
    ];

    $form['authentication']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('The application ID that is assigned to your app. You can find this information in the portal where you registered your app.'),
      '#required' => TRUE,
      '#default_value' => $config->get('client_id'),
    ];

    $form['authentication']['scope'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scope'),
      '#description' => $this->t('The value passed for the scope parameter in this request should be the resource identifier (application ID URI) of the resource you want, affixed with the .default suffix.'),
      '#required' => TRUE,
      '#default_value' => $config->get('scope'),
    ];

    $form['authentication']['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#description' => $this->t('The client secret that you generated for your app in the app registration portal.'),
      '#required' => TRUE,
      '#default_value' => $config->get('client_secret'),
    ];

    // Purge endpoint settings.
    $form['uri_params'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Azure CDN/Frontdoor purge endpoint settings'),
    ];

    $form['uri_params']['cdn_description'] = [
      '#type' => 'item',
      '#markup' => '<div>' . $this->t('Please refer to <a href="https://docs.microsoft.com/en-us/rest/api/cdn/endpoints/purge-content#uri-parameters" target="_blank">URI parameters for purging content</a> with Azure CDN.') . '</div>',
      '#states' => [
        'visible' => [
          ':input[name="endpoint_type"]' => ['value' => 'endpoints'],
        ],
      ],
    ];

    $form['uri_params']['frontdoor_description'] = [
      '#type' => 'item',
      '#markup' => '<div>' . $this->t('Please refer to <a href="https://docs.microsoft.com/en-us/rest/api/frontdoorservice/frontdoor/endpoints/purge-content" target="_blank">URI parameters for purging content</a> with Azure Frontdoor.') . '</div>',
      '#states' => [
        'visible' => [
          ':input[name="endpoint_type"]' => ['value' => 'afdEndpoints'],
        ],
      ],

    ];

    $form['uri_params']['endpoint_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint name'),
      '#description' => $this->t('Name of the endpoint under the profile which is unique globally.'),
      '#required' => TRUE,
      '#default_value' => $config->get('endpoint_name'),
    ];

    $form['uri_params']['profile_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Profile name'),
      '#description' => $this->t('Name of the CDN profile which is unique within the resource group.'),
      '#required' => TRUE,
      '#default_value' => $config->get('profile_name'),
    ];

    $form['uri_params']['resource_group_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Resource group name'),
      '#description' => $this->t('Name of the Resource group within the Azure subscription.'),
      '#required' => TRUE,
      '#default_value' => $config->get('resource_group_name'),
    ];

    $form['uri_params']['subscription_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subscription ID'),
      '#description' => $this->t('Azure Subscription ID.'),
      '#required' => TRUE,
      '#default_value' => $config->get('subscription_id'),
    ];

    $form['uri_params']['api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API version'),
      '#description' => $this->t('Version of the API to be used with the client request.'),
      '#required' => TRUE,
      '#default_value' => $config->get('api_version'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // General config.
    $endpoint_type = $form_state->getValue('endpoint_type');
    $devel = $form_state->getValue('devel');
    $chunk_size = $form_state->getValue('chunk_size');
    $chunk_delay = $form_state->getValue('chunk_delay');

    // Authentication config.
    $tenant_id = $form_state->getValue('tenant_id');
    $client_id = $form_state->getValue('client_id');
    $scope = $form_state->getValue('scope');
    $client_secret = $form_state->getValue('client_secret');

    // Purge API config.
    $endpoint_name = $form_state->getValue('endpoint_name');
    $profile_name = $form_state->getValue('profile_name');
    $resource_group_name = $form_state->getValue('resource_group_name');
    $subscription_id = $form_state->getValue('subscription_id');
    $api_version = $form_state->getValue('api_version');

    $this->config('azure_cdn_purge.settings')
      ->set('endpoint_type', $endpoint_type)
      ->set('devel', $devel)
      ->set('chunk_size', $chunk_size)
      ->set('chunk_delay', $chunk_delay)
      ->set('tenant_id', $tenant_id)
      ->set('client_id', $client_id)
      ->set('scope', $scope)
      ->set('client_secret', $client_secret)
      ->set('endpoint_name', $endpoint_name)
      ->set('profile_name', $profile_name)
      ->set('resource_group_name', $resource_group_name)
      ->set('subscription_id', $subscription_id)
      ->set('api_version', $api_version)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
