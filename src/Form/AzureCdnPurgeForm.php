<?php

namespace Drupal\azure_cdn_purge\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface;
use Drupal\purge\Plugin\Purge\Processor\ProcessorsServiceInterface;
use Drupal\purge\Plugin\Purge\Purger\PurgersServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Azure CDN settings for this purger.
 */
class AzureCdnPurgeForm extends ConfigFormBase {

  /**
   * The 'purge.invalidation.factory' service.
   *
   * @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface
   */
  protected $purgeInvalidationFactory;

  /**
   * The 'purge.processors' service.
   *
   * @var \Drupal\purge\Plugin\Purge\Processor\ProcessorsServiceInterface
   */
  protected $purgeProcessors;

  /**
   * The 'purge.purgers' service.
   *
   * @var \Drupal\purge\Plugin\Purge\Purger\PurgersServiceInterface
   */
  protected $purgePurgers;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface $purge_invalidation_factory
   *   The purge invalidation factory service.
   * @param \Drupal\purge\Plugin\Purge\Processor\ProcessorsServiceInterface $purge_processors
   *   The purge processors service.
   * @param \Drupal\purge\Plugin\Purge\Purger\PurgersServiceInterface $purge_purgers
   *   The purge purgers service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Core messenger service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    InvalidationsServiceInterface $purge_invalidation_factory,
    ProcessorsServiceInterface $purge_processors,
    PurgersServiceInterface $purge_purgers,
    MessengerInterface $messenger,
  ) {
    parent::__construct($config_factory);
    $this->purgeInvalidationFactory = $purge_invalidation_factory;
    $this->purgeProcessors = $purge_processors;
    $this->purgePurgers = $purge_purgers;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('purge.invalidation.factory'),
      $container->get('purge.processors'),
      $container->get('purge.purgers'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'azure_cdn_purge_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'azure_cdn_purge.purge_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Purge settings.
    $form['manual_purge'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Manual purge'),
    ];

    $form['manual_purge']['description'] = [
      '#markup' => '<div>' . $this->t('This form allow you to submit manual purge requests to Azure CDN/Frontdoor.') . '</div>',
    ];

    $form['manual_purge']['paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Paths to purge'),
      '#rows' => 5,
      '#description' => $this->t('Enter one path per line, wildcards are supported.'),
      '#required' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Purge'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // General config.
    $paths = $form_state->getValue('paths');
    $items = explode("\n", $paths);

    $purgeInvalidationFactory = $this->purgeInvalidationFactory;
    $purgeProcessors = $this->purgeProcessors;
    $purgePurgers = $this->purgePurgers;

    $processor = $purgeProcessors->get('azure_processor');
    $invalidations = [];

    foreach ($items as $item) {
      if (strpos($item, '*') !== FALSE) {
        $invalidations[] = $purgeInvalidationFactory->get('wildcardpath', ltrim($item, '\/'));
      }
      else {
        $invalidations[] = $purgeInvalidationFactory->get('path', ltrim($item, '\/'));
      }
    }

    try {
      $purgePurgers->invalidate($processor, $invalidations);
      $this->messenger->addMessage($this->t('Purge request successfully sent to Azure CDN/Frontdoor.'));
    }
    catch (\Exception $e) {
      // Error.
    }
  }

}
