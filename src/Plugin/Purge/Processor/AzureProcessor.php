<?php

namespace Drupal\azure_cdn_purge\Plugin\Purge\Processor;

use Drupal\purge\Plugin\Purge\Processor\ProcessorBase;
use Drupal\purge\Plugin\Purge\Processor\ProcessorInterface;

/**
 * Processor for the 'InvalidationManager->invalidate' function.
 *
 * @PurgeProcessor(
 *   id = "azure_processor",
 *   label = @Translation("Azure CDN Processor"),
 *   description = @Translation("Processor for manual Azure CDN purge."),
 *   enable_by_default = true,
 *   configform = "",
 * )
 */
class AzureProcessor extends ProcessorBase implements ProcessorInterface {}
