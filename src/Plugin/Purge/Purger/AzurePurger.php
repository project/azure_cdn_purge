<?php

namespace Drupal\azure_cdn_purge\Plugin\Purge\Purger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use Drupal\purge\Plugin\Purge\Purger\PurgerInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Azure CDN purger.
 *
 * @PurgePurger(
 *   id = "azurecdn",
 *   label = @Translation("Azure CDN"),
 *   description = @Translation("Purger for Azure CDN."),
 *   types = {"path", "wildcardpath"},
 *   multi_instance = FALSE,
 * )
 */
class AzurePurger extends PurgerBase implements PurgerInterface {

  /**
   * The settings configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('logger.factory')->get('azurecdn')
    );
  }

  /**
   * Constructs a \Drupal\Component\Plugin\AzurePurger.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->config = $config_factory->get('azure_cdn_purge.settings');
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function routeTypeToMethod($type) {
    $methods = [
      'path'  => 'invalidate',
      'wildcardpath' => 'invalidate',
    ];

    return $methods[$type] ?? 'invalidate';
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    $chunks = array_chunk($invalidations, $this->config->get('chunk_size'));
    $delay = $this->config->get('chunk_delay');

    $has_invalidations = count($invalidations) > 0;
    if (!$has_invalidations) {
      return;
    }

    $first_chunk = TRUE;
    foreach ($chunks as $chunk) {
      if (!$first_chunk) {
        sleep($delay);
      }
      $this->purgeChunk($chunk);
      $first_chunk = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasRuntimeMeasurement() {
    return TRUE;
  }

  /**
   * Purges a chunk of tags.
   *
   * @param array $invalidations
   *   Chunks of purge module invalidation objects to purge from Azure CDN.
   */
  private function purgeChunk(array &$invalidations) {
    $targets_to_purge = [];

    foreach ($invalidations as $invalidation) {
      $invalidation->setState(InvalidationInterface::PROCESSING);
      $targets_to_purge[] = '/' . $invalidation->getExpression();
    }

    try {
      $invalidation_type = $invalidations[0]->getPluginId();

      if ($invalidation_type == 'path') {
        // Get Azure URI parameters.
        $endpoint_type = $this->config->get('endpoint_type');
        $devel = $this->config->get('devel');
        $endpoint_name = $this->config->get('endpoint_name');
        $profile_name = $this->config->get('profile_name');
        $resource_group_name = $this->config->get('resource_group_name');
        $subscription_id = $this->config->get('subscription_id');
        $api_version = $this->config->get('api_version');
        $tenant_id = $this->config->get('tenant_id');
        $client_id = $this->config->get('client_id');
        $scope = $this->config->get('scope');
        $client_secret = $this->config->get('client_secret');

        $auth_url = 'https://login.microsoftonline.com/' . $tenant_id . '/oauth2/v2.0/token';
        $auth_data = [
          'form_params' => [
            'client_id' => $client_id,
            'scope' => $scope,
            'client_secret' => $client_secret,
            'grant_type' => 'client_credentials',
          ],
        ];

        $client_auth = \Drupal::httpClient();
        $request_auth = $client_auth->post($auth_url, $auth_data);
        $response_auth = json_decode($request_auth->getBody(), TRUE);

        if ($devel) {
          $this->logger->debug(json_encode($response_auth));
        }

        $access_token = $response_auth['access_token'];

        $url = "https://management.azure.com/subscriptions/" . $subscription_id . "/resourceGroups/" . $resource_group_name . "/providers/Microsoft.Cdn/profiles/" . $profile_name . "/$endpoint_type/" . $endpoint_name . "/purge?api-version=" . $api_version;

        $body_contents = [
          'contentPaths' => $targets_to_purge,
        ];

        $post_data = [
          'body' => json_encode($body_contents),
          'headers' => [
            'Content-Type' => 'application/json',
            'Content-Length' => strlen(json_encode($body_contents)),
            'Authorization' => 'Bearer ' . $access_token,
          ],
        ];

        $client = \Drupal::httpClient();
        $request = $client->post($url, $post_data);
        $response = $request->getBody();

        if ($devel) {
          $this->logger->debug($response);
        }
      }

      foreach ($invalidations as $invalidation) {
        $invalidation->setState(InvalidationInterface::SUCCEEDED);
      }
    }

    catch (\Exception $e) {
      foreach ($invalidations as $invalidation) {
        $invalidation->setState(InvalidationInterface::FAILED);
      }

      // We only want to log a single watchdog error per request. This prevents
      // the log from being flooded.
      $this->logger->error($e->getMessage());
    }
  }

}
