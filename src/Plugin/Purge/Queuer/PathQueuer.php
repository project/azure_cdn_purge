<?php

namespace Drupal\azure_cdn_purge\Plugin\Purge\Queuer;

use Drupal\purge\Plugin\Purge\Queuer\QueuerBase;
use Drupal\purge\Plugin\Purge\Queuer\QueuerInterface;

/**
 * Queues paths to your Purge queue.
 *
 * @PurgeQueuer(
 *   id = "path_queuer",
 *   label = @Translation("Path queuer"),
 *   description = @Translation("Queues Drupal paths to your Purge queue."),
 *   enable_by_default = true,
 * )
 */
class PathQueuer extends QueuerBase implements QueuerInterface {}
