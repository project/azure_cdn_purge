CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

This module extends the Purge module by providing a purger and queuer
plugin for Azure CDN.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/azure_cdn_purge

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/azure_cdn_purge

REQUIREMENTS
------------

This module requires the following modules:

 * [Purge](https://www.drupal.org/project/purge)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------
 
 * Configure the user permissions in Administration » People » Permissions:

   - 'administer azure cdn purge' (Azure CDN Purger)

     Allows user to administer purge settings.

 * Setup authentication with Azure using OAuth 2 in
   Administration » Configuration » Web services » Azure CDN
   - Get the following information from the Azure portal:
      - Tenant ID
      - Client ID
      - Scope
      - Client secret
   - Please refer to [Access token request with a shared secret](https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-client-creds-grant-flow#first-case-access-token-request-with-a-shared-secret)

 * Connect to Azure CDN purge endpoint in
   Administration » Configuration » Web services » Azure CDN
   - Get the following information from the Azure portal:
      - Endpoint name
      - Profile name
      - Resource group name
      - Subscription ID
      - API version
   - Please refer to [URI parameters for purging content](https://docs.microsoft.com/en-us/rest/api/cdn/endpoints/purge-content#uri-parameters)

TROUBLESHOOTING
---------------

 * In order to troubleshoot connection issues with Azure, enable
   the debug mode in the module's configuration form at
   /admin/config/services/azure.

MAINTAINERS
-----------

Current maintainers:
 * Stephen Mulvihill (smulvih2) - https://www.drupal.org/u/smulvih2

This project has been sponsored by:
 * Open Plus - With over a decade of trusted expertise, our team of
 professionals have un-matched experience creating content architecture
 that works. We are a team of experienced digital professionals with a
 passion for enterprise content management solutions. Visit
 https://www.openplus.ca for more information.
